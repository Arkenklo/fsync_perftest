#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>

const char* journal_filename = "journal.log";

void write_journal_entry (char* entry)
{
 struct timeval t1, t2;
 double elapsedTime;
 gettimeofday(&t1, NULL); // start timer
 int fd = open (journal_filename, O_WRONLY | O_CREAT | O_APPEND, 0660);
 write (fd, entry, strlen (entry));
 write (fd, "\n", 1);
 fsync (fd); // flush to disk
 gettimeofday(&t2, NULL); // stop timer
 close (fd);
 // compute and print the elapsed time in millisec
 elapsedTime= (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
 elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
 printf( "%f", elapsedTime );
}

void main ()
{
 write_journal_entry("test");
}
