This program measures the time it takes to fsync() or fdatasync() four characters to disk. It prints the time in milliseconds.

In writes the word `test` to the file `journal.log` in the same directory.
